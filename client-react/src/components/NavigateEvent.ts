interface NavigateEvent { (to: string): void };

export default NavigateEvent;