const baseURL = '/api/rest/v1';

export const apiURL = (url: string) => `${baseURL}/${url}`;
